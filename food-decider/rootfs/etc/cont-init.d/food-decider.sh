#!/usr/bin/with-contenv bashio
# ==============================================================================
# Home Assistant Community Add-on: food-decider
# This file validates config, creates the database and configures the app key
# ==============================================================================
declare host
declare key
declare password
declare port
declare username

if bashio::config.has_value 'remote_mysql_host'; then
  if ! bashio::config.has_value 'remote_mysql_database'; then
    bashio::exit.nok \
      "Remote database has been specified but no database is configured"
  fi

  if ! bashio::config.has_value 'remote_mysql_username'; then
    bashio::exit.nok \
      "Remote database has been specified but no username is configured"
  fi

  if ! bashio::config.has_value 'remote_mysql_password'; then
    bashio::log.fatal \
      "Remote database has been specified but no password is configured"
  fi

  if ! bashio::config.exists 'remote_mysql_port'; then
    bashio::exit.nok \
      "Remote database has been specified but no port is configured"
  fi
else
  if ! bashio::services.available 'mysql'; then
     bashio::log.fatal \
       "Local database access should be provided by the MariaDB addon"
     bashio::exit.nok \
       "Please ensure it is installed and started"
  fi

  host=$(bashio::services "mysql" "host")
  password=$(bashio::services "mysql" "password")
  port=$(bashio::services "mysql" "port")
  username=$(bashio::services "mysql" "username")

  bashio::log.warning "food-decider is using the Maria DB addon"
  bashio::log.warning "Please ensure this is included in your backups"
  bashio::log.warning "Uninstalling the MariaDB addon will remove any data"

  bashio::log.info "Creating database for food-decider if required"
  mysql \
    -u "${username}" -p"${password}" \
    -h "${host}" -P "${port}" \
    -e "CREATE DATABASE IF NOT EXISTS \`food-decider\` ;"
fi

# Create API key if needed
if ! bashio::fs.file_exists "/data/food-decider/appkey.txt"; then
  bashio::log.info "Generating app key"
  key=$(php /var/www/food-decider/artisan key:generate --show)
  bashio::log.info "App Key generated: ${key}"
  mkdir -p /data/food-decider/
  touch /data/food-decider/appkey.txt
  echo "${key}" > /data/food-decider/appkey.txt
fi

if bashio::config.has_value "appkey"; then
   bashio::log.info "Setting appkey to user defined value"
   key=$(bashio::config "appkey")
   echo "${key}" > /data/food-decider/appkey.txt
   bashio::addon.option 'appkey'
fi

